/* Author: Sudharsan
 
 */


window.onload = function init() {
	const postUrl = "https://jsonplaceholder.typicode.com/posts";
	const imgUrl = "https://jsonplaceholder.typicode.com/photos";

	function data(url) {
		return new Promise((resolve, reject) => {
			const xhr = new XMLHttpRequest();
			xhr.open("GET", url, true);
			xhr.onload = () => resolve(JSON.parse(xhr.responseText));
			xhr.onerror = () => reject(xhr.statusText);
			xhr.send();
		});
	}

	data(postUrl)
		.then(resObj => {
			let body = document.querySelector("body");

			resObj.forEach(resp => {
				let container = document.createElement("div");
				container.className = "block";
				body.appendChild(container);
				let title = document.createElement("h2");
				container.appendChild(title);
				let post = document.createElement("p");
				container.appendChild(post);

				title.innerHTML = resp.title;
				post.innerHTML = resp.body;

				data(imgUrl)
					.then(imgObj => {
						imgObj.forEach(img => {
							if (resp.id === img.id) {
								let image = document.createElement("img");
								container.appendChild(image);
								image.setAttribute("src", img.url);
							}
						})
					})
					.catch(err => console.log(err));
			});
		})
		.catch(err => console.log(err));
}